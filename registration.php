<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <script src="js/script.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registration Form</title>
    <link rel="stylesheet" href="css/form.css">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header text-center bg-secondary text-light">

                    </div>
                    <div class="card-body">

                        <fieldset>

                            <legend class="title">Register Here</legend>
                            <form>

                                <!-- Email input -->
                                <div class="form-outline">
                                    <label class="form-label" for="form2Example1">Name</label>
                                    <input class="input uname" type="text" id="uName" class="form-control" />
                                </div>

                                <!-- Password input -->
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example2">Email</label>
                                    <input type="text" id="uEmail" class="form-control input uEmail" />
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example2">Mobile</label>
                                    <input type="text" id="uNumber" class="form-control input uNumber" />
                                </div>

                                <div class="form-outline gender">
                                    <label class="form-label" for="form2Example2">Gender</label>
                                    <div class="form-check form-check-inline-male">
                                        <input class="form-check-input ms-1 input" type="radio" name="gender" id="gMale"
                                            value="Male">
                                        <label class="form-check-label" style="margin-left: 30px;"
                                            for="inlineRadio1">Male</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input input" type="radio" name="gender" id="gFemale"
                                            value="Female">
                                        <label class="form-check-label" style="margin-left: 30px;"
                                            for="inlineRadio2">Female</label>
                                    </div>
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example2">Date of Birth</label>
                                    <input type="date" id="uDob" class="form-control input uDob" />
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example2">Password</label>
                                    <input type="password" id="uPassword" value=""
                                        class="form-control input password" />
                                </div>

                                <div class="form-btn">
                                    <input type="button" id="uSubmit" onclick="onvalidate()" class="button"
                                        value="Sign-up" />
                                    <!-- <button type="button" class="btn btn-outline-primary btn-mg mb-3">Sign up</button> -->
                                </div>

                                <div class="text-center">
                                    <hr>
                                    <p>Already have an account?<a href="index.php">Sign in</a></p>
                                </div>
                            </form>
                        </fieldset>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>