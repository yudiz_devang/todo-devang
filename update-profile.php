<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">   
    <title>Update profile</title>
</head>

<body onload="get()">
    <!-- Navbar start -->
    <div class="topnav">
        <a class="navbar-brand" href="#">TO DO</a>
        <a  href="dashboard.php">Home</a>
        <a href="profile.php">Profile</a>
        <a href="change-psw.php">Change Password</a>
        <a href="#" class="logout" onclick="logout()"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
    </div>
    <!-- Navbar end -->

    <div class="main">
        <h2>PROFILE-UPDATE</h2>
        <div class="card">
            <div class="card-body">
                <table>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>:</td>
                            <td><input class="txt" type="text" id="eNames"></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><input class="txt" type="text" id="eEmails" ></td>
                        </tr>
                        <tr>
                            <td>Mobile</td>
                            <td>:</td>
                            <td><input class="txt" type="text" id="eMobiles" ></td>
                        </tr>
                     
                        <tr>
                            <td><button class="btnupdate" onclick="update()">Update</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

       
    </div>
</body> 
<script src="js/profile.js"></script>

</html>