<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Form</title>
    <script src="js/script.js"></script>
    <link rel="stylesheet" href="css/form.css">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header text-center bg-secondary text-light">

                    </div>
                    <div class="card-body">

                        <fieldset>

                            <legend class="title">Login Here</legend>
                            <form class="login-form">

                                <!-- Email input -->
                                <div class="form-outline">
                                    <label class="form-label" for="form2Example1">Name</label>
                                    <input class="input uname" type="text" id="username" class="form-control" />
                                </div>

                                <!-- Password input -->
                              

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example2">Password</label>
                                    <input type="password" id="password" value=""
                                        class="form-control input password" />
                                </div>

                                <div class="form-btn">
                                    <input type="button" id="uSubmit" onclick="loginvalidate()" class="button"
                                        value="Login" />
                                    <!-- <button type="button" class="btn btn-outline-primary btn-mg mb-3">Sign up</button> -->
                                </div>

                                <div class="text-center">
                                    <hr>
                                    <p>Not a member?<a href="registration.php">Register Here</a></p>
                                </div>
                            </form>
                        </fieldset>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>