<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/todo.css">
    <script src="js/dashboard.js"></script>
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">
    <title>Dashboard</title>
</head>

<body onload="clock()">
    <!-- Navbar start -->
    <div class="topnav">
        <a class="navbar-brand" href="#">TO DO</a>
        <a href="#home">Home</a>
        <a href="profile.php">Profile</a>
        <a href="change-psw.php">Change Password</a>
        <a href="#" class="logout" onclick="logout()"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
    </div>
    <!-- Navbar end -->

    <!-- sidabar -->
    <div class="sidebar">
        <div class="row">
            <div class="col-clock">
                <div id="clock"></div>
            </div>
            <hr>
        </div>
        <div class="row-weather">
            <div class="notification"> </div>
            <div class="main_weather">
                <h1 class="w_hadding">Live Weather</h1>
                <img src="https://ssl.gstatic.com/onebox/weather/64/partly_cloudy.png" onload="weather()">
                <h2 id="temperature"></h2>
                <h2 id="description"></h2>
                <p id="location"></p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-gif">
                <img src="" id="image" height="250px" width="250px">
            </div>
        </div>

    </div>

    <!-- sidebar-end -->
    <div class="todo">
        <div class="container">
            <h1>TO DO</h1>
            <div class="wrapper">
                <div class="task-input">
                    <input type="text" placeholder="Add a new task">
                </div>
                <div class="controls">
                    <div class="filters">
                        <span class="active" id="all">All</span>
                        <span id="pending">Pending</span>
                        <span id="completed">Completed</span>
                    </div>
                    <button class="clear-btn">Clear All</button>
                </div>
                <ul class="task-box"></ul>
            </div>
        </div>
    </div>
</body>
<script src="js/todo.js"></script>
</html>