let userarr = [];
function onvalidate() {
    const name = document.getElementById('uName').value.trim();
    const email = document.getElementById('uEmail').value.trim();
    const mobile = document.getElementById('uNumber').value.trim();
    const male = document.getElementById('gMale');
    const female = document.getElementById('gFemale');
    const dob = document.getElementById('uDob').value;
    const password = document.getElementById('uPassword').value;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneformat = /^[6-9]\d{9}$/gi;

    let gender;
    gender = (() => male.checked ? 'Male' : 'Female')()

    if (name == '' || email == '' || mobile == '' || dob == '' || password == '') {
        alert('Please fill all details')
        return false;
    }
    else if (!email.match(mailformat)) {
        alert('Please enter valid email address')
    }
    else if (!mobile.match(phoneformat)) {
        alert('Plase enter valid phone number')
    }
    else if (male.checked == false && female == false) {
        alert('Please select gender');
    }
    else if (password.length < 6) {
        alert(`Password must be 6 letter.`)
    }
    else {
        let user = {
            'name': name,
            'email': email,
            'mobile': mobile,
            'gender': gender,
            'dob': dob,
            'password': password
        }
        var uarray = JSON.parse(localStorage.getItem('userdata'))

        if (uarray == null) {
            userarr.push(user);
            window.localStorage.setItem('userdata', JSON.stringify(userarr));
        }
        else {
            uarray.push(user);
            window.localStorage.setItem('userdata', JSON.stringify(uarray));
            window.location.replace('index.php');
        }
    }
}

function loginvalidate() {
    var uname = document.getElementById('username').value.trim();
    var upass = document.getElementById('password').value;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (uname == '' || upass == '') {
        alert('Please fill all details')
        return false;
    }
    else if (!uname.match(mailformat)) {
        alert('Please enter valid email address')

    }
    else if (upass.length < 6) {
        alert(`Password length must be 6 letter`)
    }

    let data = JSON.parse(localStorage.getItem('userdata'));
    for (let i = 0; i < data.length; i++) {
        var temp;
        if (uname == data[i].email && upass == data[i].password) {
            temp = true;
            sessionStorage.setItem("uname", data[i].name);
            sessionStorage.setItem("upass", data[i].password);
            sessionStorage.setItem("uindex", i);
            break;
        }
    }
    if (temp == true) {
        window.location.replace('dashboard.php')
    }
    else {
        alert('invalid credentials.')
    }
}

