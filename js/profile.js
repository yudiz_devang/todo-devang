const index = sessionStorage.getItem("uindex");
if(index == null){
    location.replace('index.php')
}
function fetchdata() {
    if (sessionStorage == '') {
        window.location.replace('index.php');
    }
    else {
        var index = sessionStorage.getItem("uindex");
        let data = JSON.parse(localStorage.getItem('userdata'));
        document.getElementById('ename').innerHTML = data[index].name;
        document.getElementById('eemail').innerHTML = data[index].email;
        document.getElementById('egender').innerHTML = data[index].gender;
        document.getElementById('emobile').innerHTML = data[index].mobile;
        document.getElementById('edob').innerHTML = data[index].dob;
    }
}
function get() {
    var index = sessionStorage.getItem("uindex");
    let data = JSON.parse(localStorage.getItem('userdata'));

    document.getElementById('eNames').value = data[index].name;
    document.getElementById('eEmails').value = data[index].email;
    document.getElementById('eMobiles').value = data[index].mobile;

}
function logout() {
    sessionStorage.clear();
    window.location.replace('index.php')
}
function update() {
    var name = document.getElementById('eNames').value;
    var email = document.getElementById('eEmails').value;
    var mobile = document.getElementById('eMobiles').value;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneformat = /^[89][0-9]{9}/;

    if (name == '' || email == '' || mobile == '') {
        alert('Please fill all details')
        return false;

    }
    else if (!email.match(mailformat)) {
        alert('Please enter valid email address')
    }
    else if (!mobile.match(phoneformat)) {
        alert('Plase enter valid phone number')
    }
    else {
        var uarray = JSON.parse(localStorage.getItem('userdata'))
        var uindex = sessionStorage.getItem("uindex");

        uarray[uindex].name = name;
        uarray[uindex].email = email;
        uarray[uindex].mobile = mobile;
        localStorage.setItem('userdata', JSON.stringify(uarray));
        alert('Data Updated')
        window.location.replace('profile.php');
    }
}

function updatepsw(){
    var cpass = document.getElementById('ecpass').value;
    var npass = document.getElementById('enpass').value;
    var pass = sessionStorage.getItem("upass");
    if (cpass == '' || npass == '') {
        alert('Please fill all details')
        return false;
    }
    else if(pass != cpass){
        alert('Current password can not match')
    }
    else{
        var uarray = JSON.parse(localStorage.getItem('userdata'))
        var uindex = sessionStorage.getItem("uindex");

        uarray[uindex].password = npass;
        
        localStorage.setItem('userdata', JSON.stringify(uarray));
        alert('Data Updated')
        window.location.replace('dashboard.php');
    }
}