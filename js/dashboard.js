const index = sessionStorage.getItem("uindex");
if(index == null){
    location.replace('index.php')
}
// Time
function clock() {
    var time = new Date();
    var hrs = time.getHours();
    var min = time.getMinutes();
    var sec = time.getSeconds();

    if (hrs > 12) {
        hrs = hrs - 12;
    }
    if (hrs == 0) {
        hrs = 12;
    }

    document.getElementById('clock').innerHTML = hrs + ':' + min + ':' + sec;
}
setInterval(clock, 500);

function logout() {
    sessionStorage.clear();
    window.location.replace('index.php')
    console.log('ssdfdsf')
}

// weather
function weather() {
    function getWeather() {
        let temperature = document.getElementById("temperature");
        let description = document.getElementById("description");
        let location = document.getElementById("location");

        let api = "https://api.openweathermap.org/data/2.5/weather";
        let apiKey = "a4f34114df09ff8466f17741eea80936";

        location.innerHTML = "Locating...";

        navigator.geolocation.getCurrentPosition(success, error);

        function success(position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;

            let url =
                api + "?lat=" + latitude + "&lon=" + longitude + "&appid=" + apiKey + "&units=imperial";

            fetch(url)
                .then(response => response.json())
                .then(data => {
                    let temp = data.main.temp;
                    // 32°F − 32 × 5/9
                    temp = (temp - 32) * (5 / 9)// convert F° to C°

                    temperature.innerHTML = Math.round(temp) + "° C";
                    location.innerHTML =
                        data.name + " (" + latitude + "°, " + longitude + "°)";
                    description.innerHTML = data.weather[0].main;
                });
        }

        function error() {
            location.innerHTML = "Unable to retrieve your location";
        }
    }

    getWeather();
}

// gif
var i = 0;
function getgif() {
    fetch("https://api.giphy.com/v1/gifs/search?q=iron+man&api_key=59GCHI7JsLlcF7PdGTUnEUL75ghSpiIX")
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            function image(image_url) {
                document.getElementById("image").src = data.data[i].images.original.url;
            }
            image(data.message);
        })
    i++;
}
getgif()
setInterval(getgif, 120000) //function call every 2 minutes
