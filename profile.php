<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Profile</title>
    <script src="js/profile.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/profile.js"></script>
  
    
</head>
<body onload="fetchdata()">
       <!-- Navbar start -->
       <div class="topnav">
        <a class="navbar-brand" href="#">TO DO</a>
        <a  href="dashboard.php">Home</a>
        <a href="profile.php">Profile</a>
        <a href="change-psw.php">Change Password</a>
        <a href="#" class="logout" onclick="logout()"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
    </div>
    <!-- Navbar end -->
    <!-- Main -->
    <div class="main">
        <h2>PROFILE</h2>
        <div class="card">
            <div class="card-body">
                <table>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>:</td>
                            <td id="ename"></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td id="eemail"></td>
                        </tr>
                        <tr>
                            <td>Mobile</td>
                            <td>:</td>
                            <td id="emobile"></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>:</td>
                            <td id="egender"></td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>:</td>
                            <td id="edob"></td>
                        </tr>
                        <tr>
                            <td><button class="btnupdate" onclick="window.location.replace('update-profile.php')">Update</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

       
    </div>
    <!-- End -->
</body>
</html>